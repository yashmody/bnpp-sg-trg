package com.adobe.training.core.models;

public class LeadModel {
	
	String leadName;
	String leadEmail;
	String leadHNI;
	
	
	public LeadModel(String leadName, String leadEmail, String leadHNI) {
		super();
		this.leadName = leadName;
		this.leadEmail = leadEmail;
		this.leadHNI = leadHNI;
	}
	
	public String getLeadName() {
		return leadName;
	}
	public String getLeadEmail() {
		return leadEmail;
	}
	public String getLeadHNI() {
		return leadHNI;
	}
	
	@Override
	public String toString() {
		return "LeadModel [leadName=" + leadName + ", leadEmail=" + leadEmail + ", leadHNI=" + leadHNI + "]";
	}

}
