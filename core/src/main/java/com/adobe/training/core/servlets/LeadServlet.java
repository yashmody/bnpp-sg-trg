package com.adobe.training.core.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.adobe.training.core.business.LeadService;
import com.adobe.training.core.models.LeadModel;

@Component(service=Servlet.class, property= {
		
		"sling.servlet.methods=GET",
		"sling.servlet.methods=POST",
		"sling.servlet.paths=/bin/leads"
		
})
public class LeadServlet extends SlingAllMethodsServlet {
	
	@Reference
	LeadService leadService;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		
		response.getWriter().print("This will show all leads in the system");
		
	}
	
	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		
		
		LeadModel model = new LeadModel(request.getParameter("name"),
				request.getParameter("email"), request.getParameter("hni").toString());
		
		leadService.storeLead(model);
		
		response.getWriter().print("Lead Inserted -> " +  model.toString());
		
		
		
	}
	
}
