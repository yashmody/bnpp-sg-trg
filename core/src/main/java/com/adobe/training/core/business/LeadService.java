package com.adobe.training.core.business;

import java.util.List;

import com.adobe.training.core.models.LeadModel;

public interface LeadService {
	
	void storeLead(LeadModel leadModel);
	LeadModel getLeadByEmail(String leadEmail);
	List<LeadModel> getAllLeads();

}
