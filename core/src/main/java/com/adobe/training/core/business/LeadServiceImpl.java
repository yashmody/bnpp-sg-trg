package com.adobe.training.core.business;

import java.util.List;

import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;

import org.apache.poi.openxml4j.exceptions.InvalidOperationException;
import org.apache.sling.jcr.api.SlingRepository;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.training.core.models.LeadModel;
import com.day.cq.commons.jcr.JcrUtil;


@Designate(ocd=LeadServiceImpl.CONFIG.class)
@Component(service=LeadService.class)

public class LeadServiceImpl implements LeadService {
	
	
	@ObjectClassDefinition(name="Lead Service Configuration")
	static @interface CONFIG{
		
		@AttributeDefinition(name="Lead Root Path", description="Root path to store leads in JCR. Defaults to /etc/leads")
		String root_path() default "/etc/leads";
		
		@AttributeDefinition(name="Read All Leads", type=AttributeType.BOOLEAN, description="if unchecked it will stop all leads to be read")
		boolean read_all_leads() default true;
		
	}
	
	@Reference
	SlingRepository repository;
	
	private static final Logger LOG = LoggerFactory.getLogger(LeadServiceImpl.class);
	
	private String rootLeadPath;
	private boolean readLeads;
	
	Session session;
	
	@Activate
	protected void activate(CONFIG config) {
		rootLeadPath = config.root_path();
		readLeads = config.read_all_leads();
		
	}

	@Override
	public void storeLead(LeadModel leadModel) {
		
//		Session session = repository.loginService(subServiceName)
		try {
			session = repository.login(new SimpleCredentials("admin","admin".toCharArray() ));
			
			Node rootNode = JcrUtil.createPath(rootLeadPath, "sling:Folder", session);
			Node leadNode = rootNode.hasNode(leadModel.getLeadEmail())?rootNode.getNode(leadModel.getLeadEmail()):rootNode.addNode(leadModel.getLeadEmail(), "sling:Folder");
			leadNode.setProperty("leadEmail", leadModel.getLeadEmail());
			leadNode.setProperty("leadName", leadModel.getLeadName());
			leadNode.setProperty("leadHNI", leadModel.getLeadHNI());
			session.save();
			
		} catch (LoginException e) {
			LOG.error("LoginException", e);
			e.printStackTrace();
		} catch (RepositoryException e) {
			LOG.error("RepositoryException", e);
			e.printStackTrace();
		} finally {
			if (session != null) session.logout();
		}
		
		

	}

	@Override
	public LeadModel getLeadByEmail(String leadEmail) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<LeadModel> getAllLeads() {
		if(!readLeads) {
			throw new InvalidOperationException("Reading Lead is Disabled");
		}
		return null;
	}

}
