package com.adobe.training.core.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import org.apache.sling.models.factory.ModelFactory;
import org.slf4j.Logger;

import com.adobe.cq.export.json.ComponentExporter;
//import com.adobe.cq.wcm.core.components.models.Title;
import com.day.cq.wcm.api.Page;
import com.drew.lang.annotations.Nullable;

/**
 * This title model extends the core title model: com.adobe.cq.wcm.core.components.models.Title
 * The core title model is used in both v1 and v2 core title components: /apps/core/wcm/components/title
 * In this model, we extend the title model to include a subtitle.
 */

@Model(adaptables=SlingHttpServletRequest.class,
	adapters= {ComponentExporter.class},
	resourceType=SampleModel.RESOURCE_TYPE,
	defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = "json")
public class SampleModel implements ComponentExporter{
	protected static final String RESOURCE_TYPE = "bnpparibas/components/content/title";
	
	@Inject
    @Named("log")
    private Logger logger;
	
	@SlingObject
	private SlingHttpServletRequest request;
	
	//Service to create a different model based on the request
	@OSGiService
	private ModelFactory modelFactory;

	//HTL global object in the model
	//Learn more  at Helpx > HTL Global Objects
	@ScriptVariable
	private Page currentPage;
	
	//property on the current resource saved from the dialog of a component
	@ValueMapValue
	private String subtitle;
	
	//Core Title model we are extending
//	private Title titleModel;
	
	//Method called when the model is initialized
	@PostConstruct
	protected void initModel(){
		//get the core title model from the request
//		titleModel = modelFactory.getModelFromWrappedRequest(request, request.getResource(), Title.class);
		
		//setup properties that are extending the title model
		if(subtitle == null){
			subtitle = "";
		}
	}
	
	@Nullable
//	public Title getTitle() {
//		return titleModel;
//	}
	
	public String getSubtitle() {	
		return subtitle;
	}
	
	public boolean isEmpty() {
		//Verify there is a subtitle
		if(!subtitle.isEmpty()) {
			return false;
		}
		//Verify a title was entered from the dialog and
		//Page.title or Page.PageTitle are not being used 
		String uniqueTitle = ""; //titleModel.getText();
		if (!uniqueTitle.equals(currentPage.getTitle())
			&& !uniqueTitle.equals(currentPage.getPageTitle())) {
			return false;
		}
		return true;
	}
	
	@Override
	public String getExportedType() {
		return request.getResource().getResourceType();
	}
}
